import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  con_passwordType: string = 'password';
  con_passwordIcon: string = 'eye-off';

  old_passwordType: string = 'password';
  old_passwordIcon: string = 'eye-off';


  oldpassword: "";
  newpassword: "";
  confirm_password: ""

  constructor(
    public global: GlobalService,
    private apiService: ApiService
  ) { }

  ngOnInit() {
  }

  hideShowPassword(type) {
    if (type == 'password') {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    } else if (type == 'old') {
      this.old_passwordType = this.old_passwordType === 'text' ? 'password' : 'text';
      this.old_passwordIcon = this.old_passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    } else {
      this.con_passwordType = this.con_passwordType === 'text' ? 'password' : 'text';
      this.con_passwordIcon = this.con_passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
  }

  changePassword() {
    if (!this.oldpassword) {
      this.global.presentToast("Old password field cannot be blank")
    } else if (!this.newpassword) {
      this.global.presentToast("Old password field cannot be blank")
    } else if (!this.confirm_password) {
      this.global.presentToast("Confrim password field cannot be blank")
    } else if (this.confirm_password != this.newpassword) {
      this.global.presentToast("Confrim password does not match with new password")
    } else {
      this.global.presentLoadingDefault();
      let details = {
        "current_password": this.oldpassword,
        "new_password":this.newpassword,
        "new_password_confirmation": this.confirm_password
      }
      this.apiService.apiWithTokenBody(details, 'user/update/password').then((success: any) => {
        console.log(success);
        if(success.status == 'success'){
          this.global.presentToast(success.message);
          this.global.presentLoadingClose()
        }else{
          this.global.presentToast(success.message);
          this.global.presentLoadingClose()
        }
       
      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        this.global.presentLoadingClose()
      })
    }
  }


}
