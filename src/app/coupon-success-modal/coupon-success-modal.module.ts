import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CouponSuccessModalPageRoutingModule } from './coupon-success-modal-routing.module';

import { CouponSuccessModalPage } from './coupon-success-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CouponSuccessModalPageRoutingModule
  ],
  declarations: [CouponSuccessModalPage]
})
export class CouponSuccessModalPageModule {}
