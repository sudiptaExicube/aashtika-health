import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerOrderDetailsPropoverPage } from './customer-order-details-propover.page';

const routes: Routes = [
  {
    path: '',
    component: CustomerOrderDetailsPropoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerOrderDetailsPropoverPageRoutingModule {}
