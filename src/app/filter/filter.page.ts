import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.page.html',
  styleUrls: ['./filter.page.scss'],
})
export class FilterPage implements OnInit {
   public bandArr:any = "";
   public listArr:any = [];
   public selectedOptions:any =[];
  constructor(
    private modalController: ModalController,
    public navParams: NavParams
  ) { 
   
    if (this.navParams.get('value')) {
      let pdata= this.navParams.get('value');
      this.listArr = pdata?.bandListData;
      this.bandArr = [...this.listArr];
      console.log(this.bandArr)
    }
  }

  ngOnInit() {}

  close(){
    this.modalController.dismiss()
  }

  

  selectOption(item) {
    console.log("hiii");
    if(this.selectedOptions.length >0){
      let found = false;
      for(let k=0; k< this.selectedOptions.length; k++){
        if(item.id == this.selectedOptions[k]){
          found = true;
          console.log('replace insert');
          //this.selectedOptions[k].isChecked = item?.isChecked
          this.selectedOptions.splice(k, 1);
          break;
        }else{
          found = false;
        }
      }
      if(found == false){
        console.log('fresh insert')
        this.selectedOptions.push(item.id)
      }
    }else{
      this.selectedOptions.push(item.id)
    }
    
  }
  handleInput(event) {
    const query = event.target.value.toLowerCase();
    this.bandArr = this.listArr.filter(d => d.name.toLowerCase().indexOf(query) > -1);
  }

  async apply() {
    console.log('old array',this.selectedOptions)
    let data = {selectedBand:this.selectedOptions };
    await this.modalController.dismiss(data);
  }

  async clearAllFilter(){
    this.selectedOptions = []
    let data = {selectedBand:this.selectedOptions };
    await this.modalController.dismiss(data);
  }
}
