import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForgotPasswordOTPPage } from './forgot-password-otp.page';

const routes: Routes = [
  {
    path: '',
    component: ForgotPasswordOTPPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForgotPasswordOTPPageRoutingModule {}
