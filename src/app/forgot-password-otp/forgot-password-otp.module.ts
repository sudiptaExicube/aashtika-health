import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgotPasswordOTPPageRoutingModule } from './forgot-password-otp-routing.module';

import { ForgotPasswordOTPPage } from './forgot-password-otp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForgotPasswordOTPPageRoutingModule
  ],
  declarations: [ForgotPasswordOTPPage]
})
export class ForgotPasswordOTPPageModule {}
