
import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../services/global.service';
import { ApiService } from '../services/api.service';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-password-otp',
  templateUrl: './forgot-password-otp.page.html',
  styleUrls: ['./forgot-password-otp.page.scss'],
})
export class ForgotPasswordOTPPage implements OnInit {
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  con_passwordType: string = 'password';
  con_passwordIcon: string = 'eye-off';




  otp: "";
  newpassword: "";
  confirm_password: ""
  verification_token:""
  constructor(
    public global: GlobalService,
    private apiService: ApiService,
    public activeRoute: ActivatedRoute,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe((params:any) => {
      this.verification_token = params.otp_token;
      console.log(this.verification_token)
    }) 
  }

  hideShowPassword(type) {
    if (type == 'password') {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }  else {
      this.con_passwordType = this.con_passwordType === 'text' ? 'password' : 'text';
      this.con_passwordIcon = this.con_passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
    }
  }

  changePassword() {
    if (!this.otp) {
      this.global.presentToast("OTP filed can\'t be blank")
    } else if (!this.newpassword) {
      this.global.presentToast("Old password field cannot be blank")
    } else if (!this.confirm_password) {
      this.global.presentToast("Confrim password field cannot be blank")
    } else if (this.confirm_password != this.newpassword) {
      this.global.presentToast("Confrim password does not match with new password")
    } else {
      this.global.presentLoadingDefault();
      let details = {
        "otp": this.otp,
        "new_password":this.newpassword,
        "new_password_confirmation": this.confirm_password,
        "verification_token": this.verification_token
      }
      this.apiService.apiWithTokenBody(details, 'otp/user-password-reset/verify').then((success: any) => {
        console.log(success);
        if(success.status == 'success'){
          this.global.presentToast(success.message);
          this.global.presentLoadingClose();
          this.navCtrl.navigateRoot(['./login-screen']);
        }else{
          this.global.presentToast(success.message);
          this.global.presentLoadingClose()
        }
       
      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        this.global.presentLoadingClose()
      })
    }
  }

  resendOTP(){
    this.apiService.apiWithTokenBody({verification_token:this.verification_token}, 'otp/user-password-reset/resend').then((success: any) => {
      console.log(success);
      if(success.status == 'success'){
        this.global.presentToast(success.message);
        this.global.presentLoadingClose();
      }else{
        this.global.presentToast(success.message);
        this.global.presentLoadingClose()
      }
     
    }).catch((err: any) => {
      console.log(err);
      this.global.presentToast(err.message);
      this.global.presentLoadingClose()
    })
  }


}

