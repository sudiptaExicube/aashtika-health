import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MainDashboardPageRoutingModule } from './main-dashboard-routing.module';

import { MainDashboardPage } from './main-dashboard.page';
import { LazyLoadImageModule } from 'ng-lazyload-image';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MainDashboardPageRoutingModule,
    LazyLoadImageModule
  ],
  declarations: [MainDashboardPage]
})
export class MainDashboardPageModule {}
