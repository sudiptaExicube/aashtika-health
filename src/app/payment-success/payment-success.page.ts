import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { GlobalService } from '../services/global.service';
@Component({
  selector: 'app-payment-success',
  templateUrl: './payment-success.page.html',
  styleUrls: ['./payment-success.page.scss'],
})
export class PaymentSuccessPage implements OnInit {

  public order_code:any=""
  msg:any;
  constructor(
    public global: GlobalService,
    private route: Router,
    public activeRoute: ActivatedRoute,
    public navCtrl:NavController
  ) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => {
      console.log("params : ", params)
      if(params["order_code"]){
        this.order_code = params["order_code"];
        this.msg = params["messge"]
        this.play()
      }
      
      // this.resturant_id = params["restaurantId"];
    })
  }

  gotoHome() {
    // this.route.navigate(['./main-dashboard']);
    this.navCtrl.pop()
  }

  play() {
    var song = new Audio();
    song.src = '../../assets/imgs/payment-done.wav';
    song.play();
  }
}
