import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ApiService } from './../services/api.service';
import { GlobalService } from './../services/global.service';
import { NavParams } from '@ionic/angular';
@Component({
  selector: 'app-rating-page',
  templateUrl: './rating-page.page.html',
  styleUrls: ['./rating-page.page.scss'],
})
export class RatingPagePage implements OnInit {
  order_id:any;
  point:any="5";
  
  review:any
  constructor(
    public modalController: ModalController,  
    public global: GlobalService,
    public apiService: ApiService, 
    
    private navParams: NavParams) { }

  ngOnInit() {
    this.order_id = this.navParams.get('order_id');
    console.log(this.order_id)
  }

  ratePoint(getpoint){
    this.point = getpoint
    
  }

  rateUs(){
    if(!this.point){
      this.global.presentToast("Please set your rating")
    }else if(!this.review){
      this.global.presentToast("Please share us few words")
    }else{
      this.global.presentLoadingDefault()
      this.apiService.apiWithTokenBody({review:this.review,rating:parseInt(this.point), order_id: this.order_id }, 'order/review-submit')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
          this.global.presentToast(success.message);
          this.global.presentLoadingClose();
          this.modalController.dismiss({
            order_id:this.order_id
          });
        } else {
          this.global.presentLoadingClose()
          this.global.presentToast(success.message);
          this.modalController.dismiss({
            order_id:this.order_id
          });
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentLoadingClose()
        this.global.presentToast(err.message);
        this.modalController.dismiss({
          order_id:this.order_id
        });
       
      })
    }
  }

  closeModal(){
    this.modalController.dismiss(null)
  }
}
