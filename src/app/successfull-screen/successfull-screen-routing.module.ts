import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuccessfullScreenPage } from './successfull-screen.page';

const routes: Routes = [
  {
    path: '',
    component: SuccessfullScreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuccessfullScreenPageRoutingModule {}
