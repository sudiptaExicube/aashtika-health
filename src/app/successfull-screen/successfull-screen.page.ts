import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { GlobalService } from '../services/global.service';
@Component({
  selector: 'app-successfull-screen',
  templateUrl: './successfull-screen.page.html',
  styleUrls: ['./successfull-screen.page.scss'],
})
export class SuccessfullScreenPage implements OnInit {

  public order_code:any=""
  public message:any=""
  constructor(
    public global: GlobalService,
    private route: Router,
    public activeRoute: ActivatedRoute,
    public navCtrl:NavController
  ) { }

  ngOnInit() {
    this.activeRoute.queryParams.subscribe(params => {
      console.log("params : ", params)
      if(params["order_code"]){
        this.order_code = params["order_code"];
        this.play()
      }
      if(params["message"]){
        this.message = params["message"];
      }

      // this.resturant_id = params["restaurantId"];
    })
  }

  gotoHome() {
    // this.route.navigate(['./main-dashboard']);
    this.navCtrl.navigateRoot(['./main-dashboard'])
  }

  play() {
    var song = new Audio();
    song.src = '../../assets/imgs/zomato.mp3';
    song.play();
  }
}
