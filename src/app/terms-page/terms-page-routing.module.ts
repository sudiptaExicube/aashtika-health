import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TermsPagePage } from './terms-page.page';

const routes: Routes = [
  {
    path: '',
    component: TermsPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TermsPagePageRoutingModule {}
