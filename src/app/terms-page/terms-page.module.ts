import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TermsPagePageRoutingModule } from './terms-page-routing.module';

import { TermsPagePage } from './terms-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TermsPagePageRoutingModule
  ],
  declarations: [TermsPagePage]
})
export class TermsPagePageModule {}
