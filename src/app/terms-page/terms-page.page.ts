import { Component, OnInit } from '@angular/core';
import { GlobalService } from './../services/global.service';
import { ApiService } from './../services/api.service';
@Component({
  selector: 'app-terms-page',
  templateUrl: './terms-page.page.html',
  styleUrls: ['./terms-page.page.scss'],
})
export class TermsPagePage implements OnInit {
  public loading = true;
  public content:any;
  constructor(public global: GlobalService,
    private apiService: ApiService,) { }

  ngOnInit() {
    this.getData()
  }

  getData() {
    this.apiService.apiWithoutToken({slug:'terms-conditions'},'cms/contents')
      .then((success: any) => {
        if (success.status == 'success') {
          console.log(success)
           this.content = success?.data?.cmscontent?.content;
           console.log(this.content)
          setTimeout(() => {
            this.loading = false
          }, 1000)
        } else {
          this.global.presentToast(success.message);
          setTimeout(() => {
            this.loading = false
          }, 1000)
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        setTimeout(() => {
          this.loading = false
        }, 1000)
      })
  }

}
