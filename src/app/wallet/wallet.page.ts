import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, NavController, AlertController } from '@ionic/angular';
import { GlobalService } from '../services/global.service';
import { MenuController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.page.html',
  styleUrls: ['./wallet.page.scss'],
})
export class WalletPage implements OnInit {
  public quickAmount:any = [
    {amount:'250',selected:false},
    {amount:'500',selected:false},
    {amount:'750',selected:false},
    {amount:'1000',selected:false}
  ]
  amount:any = ""
  walletBalance:any=0;
  remarks:any = "";
  constructor(
    platform: Platform,
    private route: Router,
    public global: GlobalService,
    private menu: MenuController,
    private apiService: ApiService,
    public navCtrl: NavController,
    public alertController: AlertController,
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.getWalletBalance(null)
  }

  getWalletBalance(event) {
    if(event == null){this.global.presentLoadingDefault()}
    this.apiService.checkWalletBal({})
      .then((success: any) => {
        console.log(success)
        if (success.status == 'success') {
          let successdata: any = success.data;
          this.walletBalance = successdata?.mainwallet
           console.log(successdata)
           if(event == null){this.global.presentLoadingClose()}else{event.target.complete();}
        } else {
          this.global.presentToast(success.message);
          if(event == null){this.global.presentLoadingClose()}else{event.target.complete();}
        }

      }).catch((err: any) => {
        console.log(err);
        this.global.presentToast(err.message);
        if(event == null){this.global.presentLoadingClose()}else{event.target.complete();}
      })
  }

  doRefresh(event){
    this.getWalletBalance(event)
  }

  selectAmount(index){
    console.log(index)
    this.quickAmount[index].selected = true;
    this.amount = this.quickAmount[index].amount
    for(let i=0; i<this.quickAmount.length; i++){
      if(i !=index){
        this.quickAmount[i].selected = false;
      }
    }
  }

  inputValCheck(){
    console.log('hiiii')
    if(this.amount.length == 0){
      for(let i=0; i<this.quickAmount.length; i++){
          this.quickAmount[i].selected = false;
      }
    }
  }

  requestNow(){
    if(this.amount >0){
      this.global.presentLoadingDefault()
      this.apiService.payoutRequest({amount:this.amount,remarks:this.remarks})
        .then((success: any) => {
          console.log(success)
          if (success.status == 'success') {
            let successdata: any = success.data;
             console.log(successdata);
             this.global.presentToast(success.message);
             this.global.presentLoadingClose();
             this.amount = "";
             this.remarks = "";
          } else {
            this.global.presentToast(success.message);
           this.global.presentLoadingClose()
          }
  
        }).catch((err: any) => {
          console.log(err);
          this.global.presentToast(err.message);
        })
    }else{
      this.global.presentToast('Amount should be greater then zero');
    }
   
}

viewHistory(){
  this.route.navigate(['./transaction-history'])
}

viewPayoutHistory(){
  this.route.navigate(['./payout-request'])
}

}
